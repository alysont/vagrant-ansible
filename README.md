# Vagrant environment for evaluation.(テスト用VAGRANT環境)

* this is for これは
    * developing/testing program. プログラム開発、テスト用
    * not for production.　プロダクション用途では使えません。
    * for mac and windows. macとwindows用です。
        * I recommend to use nfs for file sharing. swap and comment out on the shared_folder section.
    * also for ubuntu, etc(Linux distro)
    * HYPERV is not supported.(difficult X( )

* this is not optimized. これは最適化されていません。

* this environments includes follows この環境は以下のパッケージが入っています。
    * operation system
        * ubuntu 14LTS(ubuntu/trusty64)

    * programming language
        * ruby(rbenv: near latest version)
        * nodejs(ndenv: near latest version)
        * python2/pip2(use pip install virtualenv)
        * python3/pip3(use pip3 install virtualenv)
        * java(openjdk8: for elasticsearch)

    * database(it's mandatory to use SSH connection for GUI tools.) GUIツールでの接続はSSH経由にしてください。
        * mysql(latest: db/db)
        * postgresql(9.3)
        * mongodb(latest)

    * middleware
        * redis
        * memcached
        * elasticsearch

    * if port#s are conflicted, modify host port#s on Vagrantfile.

    ```
    opened ports
    3000 -> 13000 rails
    4567 -> 14567 sinatra
    5000 -> 15000 nodejs
    3306 -> 13306 mysql(use SSH via GUI tools)
    5432 -> 15432 postgres(use SSH via GUI tools)
    27000 -> 17000 mongodb(use SSH via GUI tools)

    if you use no-conflict port, change host ports.(it's good to use normally)
    ex: 3000 -> 3000
    ```

* NOTE: for postgresql: to add user, please input some command as follows.

```terminal
vagrant@vagrant-ubuntu-trusty-64:~$ sudo -i
root@vagrant-ubuntu-trusty-64:~# su - postgres
postgres@vagrant-ubuntu-trusty-64:~$ psql
psql (9.3.16)
Type "help" for help.

postgres=# CREATE ROLE db WITH CREATEDB LOGIN PASSWORD 'db';
CREATE ROLE
postgres=# \q
postgres@vagrant-ubuntu-trusty-64:~$create database db --username db
```

* Vagrant plugins
```
vagrant plugin install ....
-----------
sahara (0.0.17)
vagrant-bindfs (0.3.2)
vagrant-hostsupdater (1.0.2)
vagrant-omnibus (1.5.0)
vagrant-share (1.1.7, system)
vagrant-triggers (0.5.3)
vagrant-vbguest (0.14.1)
```

* workflow ワークフロー
    * mkdir project
    * vagrant up
    * happy coding in project folder
        * coding/git/DB GUI on the host OS コーディング、git、DBのGUI操作はホストOSで行います
        * running command for servers on the guest OS アプリケーションサーバの立ち上げ、細かい操作はゲストOSで行います。
    * vagrant reset / halt - up as necessary 必要に応じて仮想マシンをリセット vagrant reset、停止 vagrant haltします。 起動はvagrant upです。
    * if confused, use vagrant destroy and vagrant up. もしややこしくなってどうしようもないときは vagrant destroyで仮想マシンを一旦消してvagrant upで環境を入れ直します。 

# for docker(only)
* change branch to 'docker' and vagrant destroy && vagrant up
